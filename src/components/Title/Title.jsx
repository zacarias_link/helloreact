import { Component } from "react";
import TitleNote from "../TitleNote/TitleNote.jsx";

class Title extends Component {
  render() {
    return (
      <h1>
        {this.props.text}
        <TitleNote
          text="repetir ainda mais"
          feeling={this.props.feeling}
        ></TitleNote>
      </h1>
    );
  }
}
export default Title;
