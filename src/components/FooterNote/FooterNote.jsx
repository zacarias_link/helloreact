import { Component } from "react";

class FooterNote extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        {this.props.feeling}
      </div>
    );
  }
}
export default FooterNote;
