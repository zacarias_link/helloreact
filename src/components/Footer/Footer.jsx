import { Component } from "react";
import FooterNote from "../FooterNote/FooterNote.jsx";
class Footer extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <FooterNote
            text="repetir mais"
            feeling={this.props.feeling}
          ></FooterNote>
        </div>
      </div>
    );
  }
}
export default Footer;
