import { Component } from "react";

class TitleNote extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        {this.props.feeling}
      </div>
    );
  }
}
export default TitleNote;
