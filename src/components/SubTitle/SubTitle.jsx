import { Component } from "react";
import SubTitleNote from "../SubTitleNote/SubTitleNote.jsx";

class SubTitle extends Component {
  render() {
    return (
      <h2>
        {this.props.text}
        <SubTitleNote
          text="manda mais repetição"
          feeling={this.props.feeling}
        ></SubTitleNote>
      </h2>
    );
  }
}
export default SubTitle;
