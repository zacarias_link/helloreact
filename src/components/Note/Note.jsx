import { Component } from "react";

class Note extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        {this.props.feeling}
      </div>
    );
  }
}
export default Note;
