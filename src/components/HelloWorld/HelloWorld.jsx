import { Component } from "react";

class HelloWorld extends Component {
  render() {
    return <div>as vezes, deve-se dizer: olá, {this.props.name}</div>;
  }
}

export default HelloWorld;
