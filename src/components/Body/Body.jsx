import { Component } from "react";
import Note from "../Note/Note.jsx";
class Body extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <Note
            text="vou continuar praticando"
            feeling={this.props.feeling}
          ></Note>
        </div>
      </div>
    );
  }
}
export default Body;
