import { Component } from "react";

class SubTitleNote extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        {this.props.feeling}
      </div>
    );
  }
}
export default SubTitleNote;
