import logo from "./logo.svg";
import "./App.css";
import HelloWorld from "./components/HelloWorld/HelloWorld.jsx";
import Title from "./components/Title/Title.jsx";
import SubTitle from "./components/SubTitle/SubTitle.jsx";
import Body from "./components/Body/Body.jsx";
import Footer from "./components/Footer/Footer.jsx";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Title text="aprendendo React" feeling="<3"></Title>
        <SubTitle text="Exercicio sobre props" feeling="<3"></SubTitle>
        <Body text="pratica leva a perfeição" feeling="<3"></Body>
        <Footer text="repetição para praticar" feeling="<3"></Footer>
        <HelloWorld name="diego"></HelloWorld>
      </header>
    </div>
  );
}

export default App;
